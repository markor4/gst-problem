#pragma once

#include "dijkstra.h"
#include "mst.h"
#include "logger.h"
#include <string>
#include <unordered_map>
#include <algorithm>

using Tree = std::pair<std::pair<int, std::vector<int>>, std::vector<std::pair<int, int>>>;
using State = std::pair<Tree, int>;

bool equalVectors(std::vector<int> v1, std::vector<int> v2) {
  std::sort(v1.begin(), v1.end());
  std::sort(v2.begin(), v2.end());
  return v1 == v2;
}

bool binarySearch(const std::vector<int> &v1, int target) {
  int low{0}, mid{}, high{};
  high = v1.size() - 1;
  while (low <= high) {
    mid = (low + high) / 2;
    if (target < v1[mid]) high = mid - 1;
    else if (target > v1[mid]) low = mid + 1;
    else return true;
  }
  return false;
}

bool findLabelInVector(const std::vector<int> &v1, int target) {
  for (auto label : v1) {
    if (label == target) {
      return true;
    }
  }
  return false;
}

bool findPairInVector(const std::vector<std::pair<int, int>> &v,const std::pair<int,int> &target) {
  for (const auto &edge : v)
    if ((edge.first == target.first && edge.second == target.second) || (edge.first == target.second && edge.second == target.first))
      return true;
  return false;
}

bool findIntInVector(const std::vector<int> &v, int key) {
  for (int n : v) {
    if (n==key) {
      return true;
    }
  }
  return false;
}

std::vector<int> unionOfVectors(std::vector<int> &v1, std::vector<int> &v2) {
  if (v1.size() > v2.size()) {
    std::vector<int> result(v1);
    for (int a : v2) {
      if (!findIntInVector(v1, a)) {
        result.push_back(a);
      }
    }
    return result;
  } else {
    std::vector<int> result(v2);
    for (int a : v1) {
      if (!findIntInVector(v2, a)) {
        result.push_back(a);
      }
    }
    return result;
  }
}

void unionTreeAndShortestPath(Tree &tree, std::vector<std::pair<int,int>> &shortestPath, int V) {
  for (auto &edge : shortestPath) {
    if (edge.first < V && edge.second < V && !findPairInVector(tree.second, edge) ) {
      tree.second.push_back(edge);
    }
  }
}

Tree unionTreeAndTree(Tree &tree1, Tree &tree2, int V){
  Tree resultTree = {{}, tree1.second};
  for (auto &edge : tree2.second) {
    if (edge.first < V && edge.second < V && !findPairInVector(tree1.second, edge) ) {
      resultTree.second.push_back(edge);
    }
  }
  return resultTree;
}

int calculateWeightOfTree(Tree &tree, std::vector<std::vector<vertexWeight>> &graph, int V, const std::vector<std::vector<int>> &weightMatrix) {
  int weight = 0;
  for (auto &edge : tree.second) {
    if (edge.first >= V || edge.second >= V) {
      // don't do anything
    } else {
      int w = weightMatrix[edge.first][edge.second];
      if (w == -1) {
        exit(EXIT_FAILURE);
      }
      weight += w;
    }
  }
  return weight;
}

void allPossibleSubsets(std::vector<int> &original, std::vector<std::vector<int>> &resulting, std::vector<int >&subset, int index) {
  resulting.push_back(subset);
  for (int i = index; i < original.size(); i++) {
    subset.push_back(original[i]);
    allPossibleSubsets(original, resulting, subset, i + 1);
    subset.pop_back();
  }
  return;
}


// TODO: this could be more optimized
bool findStateInQueue (std::priority_queue<std::pair<Tree, int>, std::vector<std::pair<Tree, int>>, compareStates> &queue, Tree &tree, int cost) {
  std::priority_queue<std::pair<Tree, int>, std::vector<std::pair<Tree, int>>, compareStates> helperQueue{};
  bool found = false;
  while(!queue.empty()) {
    State temporary = queue.top();
    if (temporary.first.first.first == tree.first.first && equalVectors(temporary.first.first.second, tree.first.second)) {
      found = true;
      if (cost < temporary.second) {
        temporary.first = tree;
        temporary.second = cost;
      }
    }
    helperQueue.push(temporary);
    queue.pop();
  }
  while(!helperQueue.empty()) {
    queue.push(helperQueue.top());
    helperQueue.pop();
  }
  return found;
}

void treeGrow(Tree &tree, std::pair<int,int> &edge, int V, std::vector<std::vector<int>> &setOfLabelsAssociatedWithNode, std::vector<int> &setOfQueryLabels) {
    if (!findPairInVector(tree.second, edge) && edge.first < V && edge.second < V ) {
      tree.second.push_back(edge);
    }
    tree.first.first = edge.first;
    for (auto label : setOfLabelsAssociatedWithNode[edge.first]) {
      if(findLabelInVector(setOfQueryLabels, label) && !findLabelInVector(tree.first.second, label)) {
        tree.first.second.push_back(label);
      }
    }
}


void update(
  std::priority_queue<std::pair<Tree, int>, std::vector<std::pair<Tree, int>>, 
  compareStates> &queue, 
  std::vector<State> &D, 
  Tree &tree, 
  int cost, 
  int &best, 
  const std::vector<int> &labels
  ) {
  for (auto &t : D) {
    if(t.first.first.first == tree.first.first && equalVectors(t.first.first.second, tree.first.second)) {
      return;
    }
  }
  if (cost > best) {
    return;
  }
  if (equalVectors(labels, tree.first.second)) {
    best = best <= cost ? best : cost;
  }
  if (!findStateInQueue(queue, tree, cost)) {
    queue.push(std::make_pair(tree, cost));
  }
  return;
}


int gst(
  int V,
  int P,
  int S,
  std::vector<std::vector<vertexWeight>> &graph, 
  const std::vector<int> &setOfAllLabels, 
  std::vector<int> &setOfQueryLabels,
  std::vector<std::vector<int>> &setOfLabelsAssociatedWithNode,
  const std::vector<std::vector<int>> &groupOfNodesContainingLabel,
  std::vector<std::vector<int>> &weigthMatrix,
  const std::ofstream &file) {

  // matrix with dimensions P*V*N where N is the appropriate number of pairs that makes the shortest pair to the virtual node with all other nodes
  std::vector<std::vector<std::vector<std::pair<int, int>>>> pathsToVirtualNodes(P, std::vector<std::vector<std::pair<int,int>>>(V, std::vector<std::pair<int,int>>()));
  std::unordered_map<int, int> mapQueryLabelToVirtualNode{}, mapIndexToQueryLabel{}, mapQueryLabelToIndex{};
  
  for (int i=0; i<P; i++) {
    mapQueryLabelToVirtualNode.emplace(setOfQueryLabels[i], V+i); // map virtual node to corresponding label
    mapIndexToQueryLabel.emplace(i, setOfQueryLabels[i]);
    mapQueryLabelToIndex.emplace(setOfQueryLabels[i], i);
  }

  // we are cloning original graph and changing structure of the original graph by adding virtual nodes for future using
  std::vector<std::vector<vertexWeight>> artificialGraph(V+P, std::vector<vertexWeight>());
  for (int i=0; i<V; i++) {
    artificialGraph[i] = graph[i];
  }
  for (auto p : setOfQueryLabels) {
    for (int node : groupOfNodesContainingLabel[p]) {
      artificialGraph[node].push_back({mapQueryLabelToVirtualNode[p], 0});
      artificialGraph[mapQueryLabelToVirtualNode[p]].push_back({node, 0});
    }
  }
  // preprocessing by invoking Dijsktra algorithm to calculate the shortest path from virtual nodes to all other nodes
  for (int i=0; i < P; i++) {
    dijkstra(artificialGraph, V+P, V+i, pathsToVirtualNodes[i], V);
  }

  std::priority_queue<std::pair<Tree, int>, std::vector<std::pair<Tree, int>>, compareStates> Q;
  std::vector<std::pair<Tree, int>> D;
  int best = MAX;
  for (int i=0; i<V; i++) {
    for (int p : setOfLabelsAssociatedWithNode[i]) {
      if (findLabelInVector(setOfQueryLabels, p)) {
        State temp = {{{i, {p}}, {}}, 0};
        Q.push(temp);
      }
    }
  }
  while (!Q.empty()) {
    bool continueWhileLoop = false;
    State temp = Q.top();
    Q.pop();
    if (equalVectors(temp.first.first.second, setOfQueryLabels)) {
      return temp.second;
    }
    D.push_back(temp);
    std::vector<int> restOfLabels;
    for (int a : setOfQueryLabels) {
      if (!findLabelInVector(temp.first.first.second, a)) {
        restOfLabels.push_back(a);
      }
    }
    Tree newTree = {{temp.first.first.first, restOfLabels}, {}};
    for (int p : restOfLabels) {
      unionTreeAndShortestPath(newTree, pathsToVirtualNodes[mapQueryLabelToIndex[p]][temp.first.first.first], V);
    }
    Tree unitedTree = unionTreeAndTree(temp.first, newTree, V);
    unitedTree.first = {temp.first.first.first, setOfQueryLabels};
    int weightOfTree = mst(V, P, graph, unitedTree, weigthMatrix);
    if (weightOfTree < best){
      best = weightOfTree;
    }
    std::cout << "Reporting ratio: " << float(best)/float(temp.second) << std::endl;

    for(auto label: setOfLabelsAssociatedWithNode[newTree.first.first]) {
      newTree.first.second.push_back(label);
    }
    for (auto &knownState : D) {
      if (knownState.first.first.first == newTree.first.first && equalVectors(knownState.first.first.second, newTree.first.second)) {
        Tree nT = unionTreeAndTree(temp.first, knownState.first, V);
        nT.first = {knownState.first.first.first, setOfQueryLabels};
        // we don't have to calculateWeightOfTree explicitly because we can do that in unionTreeAndTree procedure
        // but it won't effect much on performance because it's running time is less that the unionTreeAndTree procedure
        update(Q, D, nT, calculateWeightOfTree(nT, graph, V, weigthMatrix), best, setOfQueryLabels);
        continueWhileLoop = true;
        break;
      }
    }
    if(continueWhileLoop) {
      continue;
    }

    if (temp.second < best/2) {
      for (auto &nodeWeight : graph[temp.first.first.first]) {
        Tree temporary = temp.first;
        std::pair<int,int> edge = std::make_pair(nodeWeight.first, temp.first.first.first);
        treeGrow(temporary, edge, V, setOfLabelsAssociatedWithNode, setOfQueryLabels);
        // calculating weight of tree that we've got by tree growing procedure is not necessary because we can do it already in the 
        // tree grow procedure
        update(Q, D, temporary, calculateWeightOfTree(temporary, graph, V, weigthMatrix), best, setOfQueryLabels);
      }
      std::vector<std::vector<int>> allsubsets{};
      std::vector<int> subsetHelper{};
      allPossibleSubsets(restOfLabels, allsubsets, subsetHelper, 0);
      for (auto &subset : allsubsets ) {
        for (auto &knownState : D) {
          if (subset.size() != restOfLabels.size() && knownState.first.first.first == temp.first.first.first 
              && equalVectors(knownState.first.first.second, subset) && !equalVectors(knownState.first.first.second, temp.first.first.second)) {
            Tree newTemporaryTree = unionTreeAndTree(temp.first, knownState.first, V);
            newTemporaryTree.first.first = temp.first.first.first;
            newTemporaryTree.first.second = unionOfVectors(knownState.first.first.second, temp.first.first.second);
            // as we are doing unionTreeAndTree, the same conclusion would be as mentioned previously for similar process
            int newWeight = calculateWeightOfTree(newTemporaryTree, graph, V, weigthMatrix);
            if (float(newWeight) <= float((2/3)*best)) {
              update(Q, D, newTemporaryTree, newWeight, best, setOfQueryLabels);
            }
          }
        }
      }
    }
  }
  return MAX;
}
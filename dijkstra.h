#pragma once

#include <queue>
#include <vector>
#include <iostream>
#include <utility>
#include "logger.h"
#include <string>

#define MAX __INT_MAX__
using vertexWeight = std::pair<int, int>;

/**
 * When calling this, we need to make sure we are making the shortest path from virtual nodes to all other nodes in original graph
 * @param V Number of nodes in graph
 * @param graph Graph to call dijsktra algorithm
 * @param originalV Number of nodes in original graph 
 */
void dijkstra(const std::vector<std::vector<vertexWeight>> &graph, int V, int source, std::vector<std::vector<std::pair<int,int>>> &pathsToVirtualNodes, int originalV) { 
  std::priority_queue<std::pair<int,int>, std::vector<std::pair<int,int>>, std::greater<std::pair<int,int>>> pq; 
  std::vector<int> distances(V, MAX);
  std::vector<int> parent(V, -1);
  std::vector<bool> traversed(V, false);
  pq.push(std::make_pair(0, source));
  distances[source] = 0;
  // here we will be sure that we don't take in account other virtual nodes
  for (int i = originalV; i < V; i++) {
    traversed[i] = true;
  }
  while (!pq.empty()) { 
    int vertex_u = pq.top().second; 
    pq.pop(); 
    traversed[vertex_u] = true;

    for (auto &x : graph[vertex_u]) {
      int vertex_v = x.first;
      int weight = x.second;

      if (!traversed[vertex_v] && distances[vertex_v] > distances[vertex_u] + weight) { 
        distances[vertex_v] = distances[vertex_u] + weight; 
        parent[vertex_v] = vertex_u;
        pq.push(std::make_pair(distances[vertex_v], vertex_v)); 
      }
    } 
  }
  for (int i = 0; i < originalV; i++) {
    if (i != source ) {
      int target = i, p = parent[i];
      if (p == -1) {
        continue;
      }
      pathsToVirtualNodes[i].push_back(std::make_pair(target, p));
      while (p != source ){
        target = p;
        p = parent[p];
        if (p == -1) {
          break;
        }
        pathsToVirtualNodes[i].push_back(std::make_pair(target, p));
      }
    }
  }
}
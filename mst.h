#pragma once

#include "gst.h"
#include "dijkstra.h"

int mst(int V, int P, std::vector<std::vector<vertexWeight>> &graph, Tree& tree, const std::vector<std::vector<int>> &weightMatrix) {
  // we need to preprocess this, we need to make artificial graph with edges from the tree and vertices from the graph + virtual nodes
  // then we can make minimum spanning tree
  std::vector<std::vector<vertexWeight>> artificialGraph(V, std::vector<vertexWeight>());
  int source = tree.first.first;
  for (const auto &edge : tree.second) {
    if (source == -1) {
      source = edge.first;
    }
    int weight = weightMatrix[edge.first][edge.second];
    if (weight < 0) {
      exit(EXIT_FAILURE);
    }
    artificialGraph[edge.first].push_back(std::make_pair(edge.second, weight));
    artificialGraph[edge.second].push_back(std::make_pair(edge.first, weight));
  }

  std::priority_queue<std::pair<int,int>, std::vector<std::pair<int,int>>, std::greater<std::pair<int,int>>> pq;
  std::vector<int> weights(V, MAX);

  std::vector<int> parent(V, -1);

  std::vector<bool> traversed(V, false);


  // part of preprocessing
  for(int i=0; i<V; i++) {
    if(artificialGraph[i].empty()) {
      traversed[i] = true;
    }
  }

  pq.push(std::make_pair(0, source));
  weights[source] = 0;

  while (!pq.empty()) {
    int vertexU = pq.top().second;
    pq.pop();
    traversed[vertexU] = true;
    for (auto &x : artificialGraph[vertexU]) {
      int vertexV = x.first;
      int w = x.second;

      if(!traversed[vertexV] && weights[vertexV] > w) {
        weights[vertexV] = w;
        pq.push(std::make_pair(weights[vertexV], vertexV));
        parent[vertexV] = vertexU;
      }
    }
  } 

  int finalWeight{0};
  tree.second = std::vector<std::pair<int,int>>();
  for (int i=0; i<V; i++) {
    if (source != i) {
      if (parent[i] != -1) {
        tree.second.push_back({i, parent[i]});
      }
      if(weights[i] != MAX) {
        finalWeight += weights[i];
      }
    }
  }
  return finalWeight;
}
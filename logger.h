#pragma once

#include <iostream>
#include <string>
#include <queue>
#include <fstream>

typedef std::pair<std::pair<int, std::vector<int>>, std::vector<std::pair<int, int>>> Tree;
typedef std::pair<Tree, int> State;

class compareStates {
  public:
    bool operator() (const State& st1, const State& st2) const {
      return st1.second > st2.second;
    }
};

void logMessage(std::string message, std::ofstream& file) {
  file << "#############################################################\n";
  file << message;
  file << "\n###########################################################\n";
  return;
}

void logEdges(const std::vector<std::pair<int,int>> &v, std::ofstream& file) {
  for (auto& e : v) {
    file << "(" << e.first  << ", " << e.second  << ");\t";
  }
  file << std::endl;
}

void logVector(const std::vector<int> &v, std::ofstream &file) {
  for (auto e : v) {
    file << e  << ";\t";
  }
  file << std::endl;
}

void logTree(const Tree &t, std::ofstream &file) {
  file << "Root: " << t.first.first << "\t";
  file << "Labels: ";
  logVector(t.first.second, file);
  file << "Edges: ";
  logEdges(t.second, file);
}

void logState(const State &s, std::ofstream &file) {
  file << "#############################################################\nCost: " << s.second << "\n";
  logTree(s.first, file);
  file << "#############################################################\n";
}

void logQueue(std::priority_queue<std::pair<Tree, int>, std::vector<std::pair<Tree, int>>, compareStates> q, std::ofstream &file) {
  logMessage("Logging out queue", file);
  while(!q.empty()) {
    State temp = q.top();
    logState(temp, file);
    q.pop();
  }
}

void logD(const std::vector<std::pair<Tree, int>> &D, std::ofstream &file) {
  logMessage("Logging out D", file);
  for (auto &st : D) {
    logState(st, file);
  }
}
int V{9};
int P{4}, S{4};

addEdge(graph, 0, 6, 4); // v1 - v7
addEdge(graph, 0, 4, 1); // v1 - v5
addEdge(graph, 1, 4, 1); // v2 - v5
addEdge(graph, 1, 6, 4); // v2 - v7
addEdge(graph, 6, 8, 1); // v7 - v9
addEdge(graph, 7, 8, 1); // v8 - v9
addEdge(graph, 2, 5, 1); // v3 - v6
addEdge(graph, 2, 7, 4); // v3 - v8
addEdge(graph, 3, 5, 1); // v4 - v6
addEdge(graph, 3, 7, 4); // v4 - v8
addEdge(graph, 4, 6, 4); // v5 - v7
addEdge(graph, 5, 7, 4); // v6 - v8

setOfLabelsAssociatedWithNode[0].push_back(0);
setOfLabelsAssociatedWithNode[1].push_back(1);
setOfLabelsAssociatedWithNode[2].push_back(2);
setOfLabelsAssociatedWithNode[3].push_back(3);

setOfQueryLabels.push_back(0);
setOfQueryLabels.push_back(1);
setOfQueryLabels.push_back(2);
setOfQueryLabels.push_back(3);

setOfAllLabels.push_back(0);
setOfAllLabels.push_back(1);
setOfAllLabels.push_back(2);
setOfAllLabels.push_back(3);

groupOfNodesContainingLabel[0].push_back(0);
groupOfNodesContainingLabel[1].push_back(1);
groupOfNodesContainingLabel[2].push_back(2);
groupOfNodesContainingLabel[3].push_back(3);
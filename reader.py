if __name__ == "__main__":
  nameOfFileToRead = "./instances/03_GRID_n=80_m=1122_k=7.stp"
  nameOfFileToWrite = "./main.cpp"

  instance = open(nameOfFileToRead, "r")
  instanceText = open(nameOfFileToWrite, "w")

  i=0

  instanceText.write(f"#include \"dijkstra.h\"\n#include \"gst.h\"\n#include \"mst.h\"\n#include <fstream>\n\n")
  instanceText.write("void addEdge(std::vector<std::vector<std::pair<int, int>>> &graph, std::vector<std::vector<int>> &weightMatrix , const int u, const int v, const int weight) {\n  graph[u].push_back({v, weight});\n  graph[v].push_back({u, weight});\n  weightMatrix[u][v] = weight;\n  weightMatrix[v][u] = weight;\n}\n\n")
  instanceText.write("int main() {\n  ")

  nodes = 0 
  k = 0

  for line in instance:
    if line.startswith("Nodes"):
      nodes = line.split()[1]
      instanceText.write(f"int V{{{nodes}}};\n  ")
    if line.startswith("Terminals"):
      k = line.split()[1]
      instanceText.write(f"int P{{{k}}}, S{{{k}}};\n  ")
      break

  instanceText.write(f"std::vector<std::vector<vertexWeight>> graph(V);\
    \r  std::vector<std::vector<int>> weightMatrix(V, std::vector<int>(V));\
    \r  std::vector<int> setOfAllLabels{{}}, setOfQueryLabels{{}};\
    \r  std::vector<std::vector<int>> setOfLabelsAssociatedWithNode(V, std::vector<int>());\
    \r  std::vector<std::vector<int>> groupOfNodesContainingLabel(S, std::vector<int>());\n  ")
  instance.close()
  instance = open(nameOfFileToRead, "r")

  for line in instance:
    # here we grab all edges
    if line.startswith("E "):
      edge = line.split()[1:]
      instanceText.write(f"addEdge(graph, weightMatrix, {edge[0]}, {edge[1]}, {edge[2]});\n  ")
    # here we grab all terminals (k)
    elif line.startswith("T "):
      instanceText.write(f"setOfAllLabels.push_back({i});\n  ")
      instanceText.write(f"setOfQueryLabels.push_back({i});\n  ")
      vertices = line.split()[1:]
      for vertex in vertices:
        instanceText.write(f"setOfLabelsAssociatedWithNode[{vertex}].push_back({i});\n  ")
      for vertex in vertices:
        instanceText.write(f"groupOfNodesContainingLabel[{i}].push_back({vertex});\n  ")
      i += 1

  instanceText.write("std::ofstream ofs;\
    \r  // ofs.open(\"logger.txt\", std::ofstream::out | std::ofstream::trunc);\
    \r  // we used this for controlling our results via terminal\
    \r  int result = gst(V, P, S, graph, setOfAllLabels, setOfQueryLabels, setOfLabelsAssociatedWithNode, groupOfNodesContainingLabel, weightMatrix, ofs);\
    \r  std::cout << \"Result: \" << result << std::endl;\
    \r  // ofs.close();\
    \r  return 0;\n}")